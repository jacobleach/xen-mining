#!/bin/bash

if ! [ -e .mining ]
then
  mkdir .mining
fi
cd .mining

echo '' > posters.txt
current_forum_page=1

echo "Fetching forum page: $current_forum_page"
curl www.sythe.org/forums/$1/ 2>/dev/null > $1-page-$current_forum_page.txt
forum_page_count=$(cat $1-page-$current_forum_page.txt | sed -n 's/.*>Page [0-9]* of \([0-9]*\)<.*/\1/p' | uniq)

while [ "$current_forum_page" -le "$forum_page_count" ]
do
  if ! [ -e $1-page-$current_forum_page.txt ]
  then
    echo "Fetching forum page: $current_forum_page"
    curl www.sythe.org/forums/$1/page-$current_forum_page 2>/dev/null > $1-page-$current_forum_page.txt
  fi

  cat $1-page-$current_forum_page.txt | 
    grep 'id="thread' | 
    grep -v -E 'class="[a-zA-Z ]*sticky[a-zA-Z ]*"' | 
    sed -n 's/.*id="thread-\([0-9]*\)".*/\1/p' > threads.txt

  for threadId in $(cat threads.txt); do
    current_page=1

    if ! [ -e thread-$threadId-page-1.txt ]
    then
      echo "Fetching page: $current_page for thread: $threadId"
      curl -L "http://www.sythe.org/threads/$threadId" 2>/dev/null > thread-$threadId-page-1.txt
    fi

    page_count=$(cat thread-$threadId-page-1.txt | sed -n 's/.*>Page [0-9]* of \([0-9]*\)<.*/\1/p' | uniq)
    if [ "$page_count" == "" ]
    then
      page_count=1
    fi

    while [ "$current_page" -le "$page_count" ]
    do
      if ! [ -e thread-$threadId-page-$current_page.txt ]
      then
        echo "Fetching page: $current_page for thread: $threadId"
        curl -L "http://www.sythe.org/threads/$threadId/page-$current_page" 2>/dev/null > thread-$threadId-page-$current_page.txt
      fi
      cat "thread-$threadId-page-$current_page.txt" | grep 'id="post' | sed -n 's/.*data-author="\(.*\)">/\1/p' >> posters.txt
      let current_page=$current_page+1
    done
  done
  let current_forum_page=$current_forum_page+1
done


cat posters.txt | sort | uniq -c | sort -rn > ../posters-with-count-$1.txt
